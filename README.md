### wrapInner() plugin

**Description:** jQuery's `.wrapInner()`, replicated in vanilla/pure JS.  
**Author:** HT (@&#x200A;glenthemes)

#### How to use:
1. Initialize the script:
```html
<script src="//js-wrapinner.gitlab.io/i/wrap.js"></script>
```

2. Run and configure (inside a `<script>`):
```javascript
wrapInner({
	selector: ".outer-container", // your div in which you want to wrap its inner contents
	innerTag: "div", // the html tag for your inner wrapper (e.g. div, span, article)
	innerClass: "inner-container" // class name for your inner wrapper
	/*---

		Using these settings, the inner wrapper will render as:

		<div class="outer-container">
			<div class="inner-container"></div>
		</div>

	---*/
})
```

#### Usage notes:
* all 3 parameters (`selector`, `innerTag`, `innerClass`) need to be filled in order for the function to execute properly.
* you can use `wrapInner()` separately on multiple different selectors, e.g.:
```javascript
// first selector
wrapInner({
	selector: ".section-one",
	innerTag: "div",
	innerClass: "section-one-inner"
})

// second/another selector
wrapInner({
	selector: ".section-two",
	innerTag: "article",
	innerClass: "section-two-inner"
})
```

#### Issues & Troubleshooting:
[discord.gg/RcMKnwz](https://discord.gg/RcMKnwz)
