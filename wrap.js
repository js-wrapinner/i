/*-----------------------------------------
    
    jQuery .wrapInner() to pure JS
    - written by HT (@glenthemes)
	
    gitlab.com/js-wrapinner/i
    
-----------------------------------------*/

window.wrapInner = function(params){
    let wrapInnerInit = (params) => {
        let sel = params.selector.trim();
        let innerTag = params.innerTag.trim();
        let innerSel = params.innerClass.trim();
        
        if(typeof(sel) !== "undefined" && typeof(innerTag) !== "undefined" && typeof(innerSel) !== "undefined"){
            innerSel = innerSel.replaceAll(".","");
            document.querySelectorAll(sel).forEach(params => {
                
                // gather all the stray text nodes
                let strayNodes = [];
                let strayText = params.childNodes;
                for(let i=0; i<strayText.length; i++){
                    if(strayText[i].nodeType === 3){
                        strayNodes.push(strayText[i]);
                    }
                }
                
                // wrap stray text nodes in <span>s
                strayNodes.forEach(dpmnt => {
                    let okdcg = document.createElement("span");
                    dpmnt.replaceWith(okdcg);
                    okdcg.appendChild(dpmnt)
                })
                
                // create the inner wrapper
                let makeInner = document.createElement(innerTag);
                makeInner.classList.add(innerSel);
                params.prepend(makeInner);
                
                params.querySelectorAll(":scope > *").forEach(children => {
                    // if(!children.matches("." + innerSel)){
                    if(!children.matches("." + innerSel)){
                    
                        // move everyone into inner wrapper
                        makeInner.append(children);
                        
                        // find descendant <span>s
                        // target the ones that are " "
                        // unwrap them into html whitespaces
                        makeInner.querySelectorAll("span").forEach(spans => {
                            if(spans.textContent.trim() == ""){
                                spans.replaceWith(...spans.childNodes);
                            }
                            
                        })
                        
                    }
                })
            })
        }
    }//end wrapInnerInit

    document.readyState == "loading" ?
    document.addEventListener("DOMContentLoaded", () => wrapInnerInit(params)) :
    wrapInnerInit(params);
}//end function